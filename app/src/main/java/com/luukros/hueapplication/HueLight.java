package com.luukros.hueapplication;

import android.content.Context;

import com.android.volley.Request;

/**
 * Created by Luuk on 27-10-2016.
 */

public class HueLight {

    // Represents the id of an individual light
    public String id;

    // Represents the state of an individual light (active true/false)
    public boolean active;

    // Represent various settings of an individual light
    public int brightness;
    public int hue;
    public int saturation;

    private VolleyRequestManager volleyRequestManager;

    public HueLight(Context context, String id, boolean active, int brightness, int hue, int saturation) {

        volleyRequestManager = VolleyRequestManager.getInstance(context);

        this.id = id;
        this.active = active;
        this.brightness = brightness;
        this.hue = hue;
        this.saturation = saturation;

    }

    public String getId() {
        return id;
    }

    public boolean getActiveState() {
        return active;
    }

    public boolean setActive(String url) {
        volleyRequestManager.doRequest(url, "{\"on\":true}", Request.Method.PUT);
        return true;
    }

    public boolean setInActive(String url) {
        volleyRequestManager.doRequest(url, "{\"on\":false}", Request.Method.PUT);
        return true;
    }

    public int getBrightness() {
        return brightness;
    }

    public boolean setBrightness(String url, int brightness) {
        volleyRequestManager.doRequest(url, "{\"bri\":" + brightness + "}", Request.Method.PUT);
        return true;
    }

    public int getHue() {
        return hue;
    }

    public boolean setHue(String url, int hue) {
        volleyRequestManager.doRequest(url, "{\"hue\":" + hue + "}", Request.Method.PUT);
        return true;
    }

    public int getSaturation() {
        return saturation;
    }

    public boolean setSaturation(String url, int saturation) {
        volleyRequestManager.doRequest(url, "{\"sat\":" + saturation + "}", Request.Method.PUT);
        return true;
    }





























}
