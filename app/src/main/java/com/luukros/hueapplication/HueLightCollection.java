package com.luukros.hueapplication;

import android.content.Context;

import java.util.ArrayList;

/**
 * Created by Luuk on 27-10-2016.
 */

public class HueLightCollection {

    private static ArrayList<HueLight> sHueLights = new ArrayList<>();
    private static HueLightCollection sInstance = null;

    public static HueLightCollection getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new HueLightCollection();
        }

        return sInstance;
    }

    // Adding an individual light to the ArrayList
    public void addLight(HueLight hueLight) {
        sHueLights.add(hueLight);
    }

    // Getting all the lights in the ArrayList
    public ArrayList<HueLight> getLights() {
        return sHueLights;
    }

    // Setting the on-state of an individual light to 'true'
    public void setActive(HueLight hueLight, String url) {
        HueLight selectedHueLight;

        for (HueLight light : sHueLights) {
            if (light.id == hueLight.id) {
                selectedHueLight = light;
                selectedHueLight.setActive(url);
                break;
            }
        }
    }

    // Setting the on-state of an individual light to 'false'
    public void setInActive(HueLight hueLight, String url) {
        HueLight selectedHueLight;

        for (HueLight light : sHueLights) {
            if (light.id == hueLight.id) {
                selectedHueLight = light;
                selectedHueLight.setInActive(url);
                break;
            }
        }
    }

    // Setting the brightness-state of an individual light to a value
    public void setBrightness(HueLight hueLight, String url, int brightness) {
        HueLight selectedHueLight;

        for (HueLight light : sHueLights) {
            if (light.id == hueLight.id) {
                selectedHueLight = light;
                selectedHueLight.setBrightness(url, brightness);
                break;
            }
        }
    }

    // Setting the hue-state of an individual light to a value
    public void setHue(HueLight hueLight, String url, int hue) {
        HueLight selectedHueLight;

        for (HueLight light : sHueLights) {
            if (light.id == hueLight.id) {
                selectedHueLight = light;
                selectedHueLight.setHue(url, hue);
                break;
            }
        }
    }

    // Setting the saturation-state of an individual light to a value
    public void setSaturation(HueLight hueLight, String url, int saturation) {
        HueLight selectedHueLight;

        for (HueLight light : sHueLights) {
            if (light.id == hueLight.id) {
                selectedHueLight = light;
                selectedHueLight.setSaturation(url, saturation);
                break;
            }
        }
    }



}
