package com.luukros.hueapplication;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.SeekBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Luuk on 27-10-2016.
 */

public class ListAdapter extends BaseAdapter {

    // Tag for logging-purposes
    private String TAG = this.getClass().getName();

    // Strings to use with the Hue-emulator
    private String username = "newdeveloper";
    private String url = "http://192.168.6.2/api/";
    private String lightsUrl = url + username + "/lights";

    // Strings to use in LA134
//    private String username = "5cd96f4231c302f33edd51c3a23e597";
//    private String url = "http://192.168.1.179/api/";
//    private String lightsUrl = url + username + "/lights";

    // Strings to use at coffee room
//    private String username = "iYrmsQq1wu5FxF9CPqpJCnm1GpPVylKBWDUsNDhB";
//    private String url = "http://145.48.205.33/api/";
//    private String lightsUrl = url + username + "/lights";

    private Context mContext;
    private LayoutInflater mLayoutInflater;

    private HueLight mHueLight;
    ArrayList mHueLights;



    RequestQueue mRequestQueue;
    VolleyRequestManager volleyRequestManager;

    public ListAdapter(Context context, LayoutInflater layoutInflater, ArrayList<HueLight> hueLights) {

        this.mContext = context;
        this.mLayoutInflater = layoutInflater;
        this.mHueLights = hueLights;

    }

    @Override
    public int getCount() {
        return mHueLights.size();
    }

    @Override
    public Object getItem(int position) {
        return mHueLights.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final ViewHolder viewHolder;

        if (convertView == null) {

            convertView = mLayoutInflater.inflate(R.layout.listview_main, null);

            viewHolder = new ViewHolder();
            // The id-TextView
            viewHolder.lightId = (TextView) convertView.findViewById(R.id.hueLightId);

            // All the label-TextViews
            viewHolder.lightBrightness = (TextView) convertView.findViewById(R.id.brightnessLabel);
            viewHolder.lightHue = (TextView) convertView.findViewById(R.id.hueLabel);
            viewHolder.lightSaturation = (TextView) convertView.findViewById(R.id.saturationLabel);

            // All the SeekBars
            viewHolder.brightnessSeekBar = (SeekBar) convertView.findViewById(R.id.brightnessSeekBar);
            viewHolder.hueSeekBar = (SeekBar) convertView.findViewById(R.id.hueSeekBar);
            viewHolder.saturationSeekBar = (SeekBar) convertView.findViewById(R.id.saturationSeekBar);

            viewHolder.brightnessSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    // Auto-generated
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {
                    // Auto-generated
                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {
                    int progress = seekBar.getProgress();
                    int brightness = (int) (progress * 2.54);
                    mHueLight.setBrightness(lightsUrl + "/state", brightness);
                }
            });

            viewHolder.hueSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    // Auto-generated
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {
                    // Auto-generated
                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {
                    int progress = seekBar.getProgress();
                    int hue = (int) (progress * 655.35);
                    mHueLight.setBrightness(lightsUrl + "/state", hue);
                }
            });

            viewHolder.saturationSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    // Auto-generated
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {
                    // Auto-generated
                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {
                    int progress = seekBar.getProgress();
                    int saturation = (int) (progress * 2.54);
                    mHueLight.setBrightness(lightsUrl + "/state", saturation);
                }
            });

            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        HueLight hueLight = (HueLight) mHueLights.get(position);
        viewHolder.lightId.setText(hueLight.getId().toString());


        return convertView;

    }



    private static class ViewHolder {

        // The id-TextView
        public TextView lightId;

        // All the label-TextViews
        public TextView lightBrightness;
        public TextView lightHue;
        public TextView lightSaturation;

        // All the SeekBars
        public SeekBar brightnessSeekBar;
        public SeekBar hueSeekBar;
        public SeekBar saturationSeekBar;

    }
}
