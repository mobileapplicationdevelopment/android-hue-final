package com.luukros.hueapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;

public class MainActivity extends AppCompatActivity {

    // Tag for logging-purposes
    private String TAG = this.getClass().getName();

//    // Strings to use with the Hue-emulator
    private String username = "newdeveloper";
    private String url = "http://192.168.1.175/api/";       // Should always be ipconfig > wireless LAN adapter Wi-Fi > IPv4 Address
    private String lightsUrl = url + username + "/lights";

    // Strings to use in LA134
//    private String username = "5cd96f4231c302f33edd51c3a23e597";
//    private String url = "http://192.168.1.179/api/";
//    private String lightsUrl = url + username + "/lights";

    // Strings to use at coffee room
//    private String username = "iYrmsQq1wu5FxF9CPqpJCnm1GpPVylKBWDUsNDhB";
//    private String url = "http://145.48.205.33/api/";
//    private String lightsUrl = url + username + "/lights";

    private ListView mListView;
    private ListAdapter mListAdapter;

    ArrayList<HueLight> hueLights = new ArrayList<>();

    VolleyRequestManager volleyRequestManager;
    HueLightCollection hueLightCollection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Creating the ListView and assigning the ListAdapter to it
        mListView = (ListView) findViewById(R.id.hueLightsList);
        mListAdapter = new ListAdapter(this, getLayoutInflater(), hueLights);
        mListView.setAdapter(mListAdapter);

        volleyRequestManager = VolleyRequestManager.getInstance(this);
        hueLightCollection = HueLightCollection.getInstance(this);

        try {
            volleyRequestManager.doGETRequest(lightsUrl, new VolleyRequestManager.VolleyCallback() {

                @Override
                public void onSuccess(String result) {

                    try {

                        JSONObject jsonObject = new JSONObject(result);

                        for (Iterator<String> stringIterator = jsonObject.keys(); stringIterator.hasNext();) {

                            String key = stringIterator.next();

                            boolean active = jsonObject.getJSONObject(key).getJSONObject("state").getBoolean("on");
                            int brightness = jsonObject.getJSONObject(key).getJSONObject("state").getInt("bri");
                            int hue = jsonObject.getJSONObject(key).getJSONObject("state").getInt("hue");
                            int saturation = jsonObject.getJSONObject(key).getJSONObject("state").getInt("sat");

                            HueLight hueLight = new HueLight(getApplicationContext(), key, active, brightness, hue, saturation);

                            hueLightCollection.addLight(hueLight);
                            hueLights.add(hueLight);

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

            });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
