package com.luukros.hueapplication;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.Iterator;
import java.util.concurrent.ExecutionException;

/**
 * Created by Luuk on 27-10-2016.
 */

public class VolleyRequestManager {

    // Tag for logging-purposes
    private String TAG = this.getClass().getName();

    private static VolleyRequestManager sInstance = null;
    private Context mContext;

    private String requestResponse;

    public VolleyRequestManager(Context context) {
        this.mContext = context;
    }

    public static VolleyRequestManager getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new VolleyRequestManager(context);
        }

        return sInstance;
    }

    public String doRequest(String requestUrl, final String requestBody, int requestMethod) {

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(requestMethod, requestUrl,
                new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.i(TAG, response);
                requestResponse = response;
            }

        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, error.toString());
                error.printStackTrace();
            }

        }) {

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {

                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while getting the bytes of %s using %s",
                            requestBody, "utf-8");
                    uee.printStackTrace();
                    return null;
                }

            }

        };

        requestQueue.add(stringRequest);

        return requestResponse;

    }

    public String doGETRequest(String requestUrl, final VolleyCallback volleyCallback)
        throws ExecutionException, InterruptedException {

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, requestUrl,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        Log.i(TAG, response);
                        volleyCallback.onSuccess(response);
                    }

                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(TAG, error.toString());
                        error.printStackTrace();
                    }
        });

        requestQueue.add(stringRequest);

        return null;

    }

//    private void getJSON(String response) {
//
//        try {
//
//            JSONObject jsonObject = new JSONObject(response);
//
//            for (Iterator<String> stringIterator = jsonObject.keys(); stringIterator.hasNext(); ) {
//
//                HueLight hueLight = new HueLight();
//                hueLight.id = stringIterator.next();
//                hueLight.active = jsonObject.getJSONObject(hueLight.id).getJSONObject("state").getBoolean("on");
//                hueLight.brightness = jsonObject.getJSONObject(hueLight.id).getJSONObject("state").getInt("bri");
//                hueLight.hue = jsonObject.getJSONObject(hueLight.id).getJSONObject("state").getInt("hue");
//                hueLight.saturation = jsonObject.getJSONObject(hueLight.id).getJSONObject("state").getInt("sat");
//
//            }
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//    }

    public interface VolleyCallback {

        void onSuccess(String result);

    }

}
